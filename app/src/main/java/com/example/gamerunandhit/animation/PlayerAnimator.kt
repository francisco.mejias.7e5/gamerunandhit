package com.example.gamerunandhit.animation

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Size
import com.example.gamerunandhit.R
import java.util.*

class PlayerAnimator(context: Context, width: Int, height: Int) {
    val idle: Animation = Animation(context,
        context.resources.obtainTypedArray(R.array.idle).toList().map {
            Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.resources, it), width, height, false)
        }
    )

    val jumping: Animation = Animation(context,
        context.resources.obtainTypedArray(R.array.jumping).toList().map {
            Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.resources, it), width, height, false)
        }
    )

    val running: Animation = Animation(context,
        context.resources.obtainTypedArray(R.array.running).toList().map {
            Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.resources, it), width, height, false)
        }
    )

    val runSlashing: Animation = Animation(context,
        context.resources.obtainTypedArray(R.array.run_slashing).toList().map {
            Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.resources, it), width, height, false)
        }
    )

    val slashing: Animation = Animation(context,
        context.resources.obtainTypedArray(R.array.slashing).toList().map {
            Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.resources, it), width, height, false)
        }
    )

    val slashingInTheAir: Animation = Animation(context,
        context.resources.obtainTypedArray(R.array.slashing_in_the_air).toList().map {
            Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.resources, it), width, height, false)
        }
    )

    val death: Animation = Animation(context,
        context.resources.obtainTypedArray(R.array.dying).toList().map {
            Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.resources, it), width, height, false)
        }
    )

    private fun TypedArray.toList(): List<Int> {
        val mutableList = mutableListOf<Int>()
        repeat(this.length()) {
            mutableList += this.getResourceId(it, 0)
        }
        return mutableList.toList()
    }
}
