package com.example.gamerunandhit.animation

import android.content.Context
import android.graphics.Bitmap
import android.util.Size
import com.example.gamerunandhit.ImageTreatment.flip
import com.example.gamerunandhit.model.Player
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*

class Animation(private val context: Context, bitmapList: List<Bitmap>) {
    var orientation: Player.Action = Player.Action.RIGHT
    var pos = 0
    var stopped = false
    var onFinishing = false
    private val frames: List<Bitmap> = bitmapList
    private val flippedFrames: List<Bitmap> = frames.map { it.flip(context.resources) }
    private val currentFrame get() = if (orientation == Player.Action.LEFT) {
        flippedFrames.getAndIncrease()
    } else {
        frames.getAndIncrease()
    }

    fun get() = currentFrame

    private fun List<Bitmap>.getAndIncrease(): Bitmap {
        val bitmap = this[pos]
        if (!stopped) {
            if (pos == frames.lastIndex) {
                pos = 0
            } else {
                pos++
            }
        }
        return bitmap
    }

    fun onFinish(function: () -> Unit) {
        if (!onFinishing) {
            onFinishing = true
            CoroutineScope(Dispatchers.Main).launch {
                if (!stopped) {
                    while (pos != frames.lastIndex) {
                        delay(2)
                    }
                    function.invoke()
                    onFinishing = false
                }
            }
        }
    }

    fun reset() {
        pos = 0
    }

    fun stop() {
        stopped = true
    }
}