package com.example.gamerunandhit

import android.annotation.SuppressLint
import android.content.res.ColorStateList
import android.content.res.Resources
import android.graphics.Color
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.util.Size
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.gamerunandhit.model.Player
import com.example.gamerunandhit.view.GameView

class GameFragment : Fragment() {
    private lateinit var gameView: GameView
    private lateinit var moveLeft: Button
    private lateinit var moveRight: Button
    private lateinit var attack: Button
    private lateinit var jump: Button

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val screenSize = getScreenSize()
        val newScreenSize = Size(screenSize.width, screenSize.height - 350)
        gameView = GameView(requireContext(), newScreenSize) {
            findNavController().navigate(R.id.action_gameFragment_to_homeFragment)
        }
        val game = FrameLayout(requireContext())
        val gameButtons = RelativeLayout(requireContext())
        val params = RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.MATCH_PARENT,
            RelativeLayout.LayoutParams.MATCH_PARENT
        )
        gameButtons.layoutParams = params
        gameButtons.addMovementButtons()
        gameButtons.actionButtons()
        game.addView(gameView)
        game.addView(gameButtons)

        return game
    }

    private fun RelativeLayout.addMovementButtons() {
        moveLeft = Button(requireContext())
        moveLeft.setBackgroundResource(R.drawable.arrow)
        moveLeft.backgroundTintList = ColorStateList.valueOf(Color.RED)
        moveLeft.rotation = 180f

        moveRight = Button(requireContext())
        moveRight.setBackgroundResource(R.drawable.arrow)
        moveRight.backgroundTintList = ColorStateList.valueOf(Color.RED)

        val leftButtonParams = RelativeLayout.LayoutParams(
            200,
            200
        )
        val rightButtonParams = RelativeLayout.LayoutParams(
            200,
            200
        )
        leftButtonParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE)
        leftButtonParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
        rightButtonParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE)
        rightButtonParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)

        leftButtonParams.setMargins(50, 0, 0, 50)

        moveLeft.layoutParams = leftButtonParams

        rightButtonParams.setMargins(350, 0, 0, 50)

        moveRight.layoutParams = rightButtonParams

        this.addView(moveLeft)
        this.addView(moveRight)
    }

    private fun RelativeLayout.actionButtons() {
        attack = Button(requireContext())
        attack.setBackgroundResource(R.drawable.attack)
        attack.backgroundTintList = ColorStateList.valueOf(Color.RED)

        jump = Button(requireContext())
        jump.setBackgroundResource(R.drawable.jump)
        jump.backgroundTintList = ColorStateList.valueOf(Color.RED)

        val attackButtonParams = RelativeLayout.LayoutParams(
            200,
            200
        )
        val jumpButtonParams = RelativeLayout.LayoutParams(
            200,
            200
        )
        attackButtonParams.addRule(RelativeLayout.ALIGN_PARENT_END, RelativeLayout.TRUE)
        attackButtonParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
        jumpButtonParams.addRule(RelativeLayout.ALIGN_PARENT_END, RelativeLayout.TRUE)
        jumpButtonParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)

        attackButtonParams.setMargins(0, 0, 50, 50)
        attack.layoutParams = attackButtonParams

        jumpButtonParams.setMargins(0, 0, 350, 50)
        jump.layoutParams = jumpButtonParams

        this.addView(attack)
        this.addView(jump)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        moveLeft.setOnTouchListener { _, motionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_DOWN) {
                gameView.start(Player.Action.LEFT)
            }
            if (motionEvent.action == MotionEvent.ACTION_UP) {
                gameView.stop(Player.Action.LEFT)
            }
            true
        }
        moveRight.setOnTouchListener { _, motionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_DOWN) {
                gameView.start(Player.Action.RIGHT)
            }
            if (motionEvent.action == MotionEvent.ACTION_UP) {
                gameView.stop(Player.Action.RIGHT)
            }
            true
        }
        attack.setOnTouchListener { _, motionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_DOWN) {
                gameView.attack()
            }
            true
        }
        jump.setOnTouchListener { _, motionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_DOWN) {
                gameView.jump()
            }
            if (motionEvent.action == MotionEvent.ACTION_UP) {
                gameView.stopJump()
            }
            true
        }
    }

    private fun getScreenSize(): Size {
        val metrics = Resources.getSystem().displayMetrics
        return Size(metrics.widthPixels, metrics.heightPixels)
    }
}
