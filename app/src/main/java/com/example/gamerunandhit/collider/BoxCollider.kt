package com.example.gamerunandhit.collider

data class BoxCollider(
    override val positionX: Float,
    override val positionY: Float,
    override val width: Int,
    override val height: Int
): Collider() {
    override fun relative(pX: Float, pY: Float, pW: Int, inverse: Boolean) = BoxCollider(((if(inverse) pW - (positionX + width) else positionX) + pX), positionY + pY, width, height)
}