package com.example.gamerunandhit.collider

sealed class Collider {
    abstract val positionX: Float
    abstract val positionY: Float
    abstract val width: Int
    abstract val height: Int

    fun onRelativeCollision(
        pCollider: Collider,
        cX: Float,
        cY: Float,
        cW: Int,
        pX: Float,
        pY: Float,
        pW: Int,
        cInverse: Boolean,
        pInverse: Boolean
    ) =
        (if(pInverse) pW - (pCollider.positionX + pCollider.width) else pCollider.positionX) + pX < ((if(cInverse) cW - (positionX + width) else positionX) + cX) + width &&
                (if(pInverse) pW - (pCollider.positionX + pCollider.width) else pCollider.positionX) + pX + pCollider.width > ((if(cInverse) cW - (positionX + width) else positionX) + cX) &&
        pCollider.positionY + pY < (positionY + cY) + height &&
                (pCollider.positionY + pY) + pCollider.height > (positionY + cY)

    abstract fun relative(pX: Float, pY: Float, pW: Int, inverse: Boolean): Collider
}