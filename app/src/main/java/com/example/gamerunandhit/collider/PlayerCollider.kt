package com.example.gamerunandhit.collider

import kotlin.math.truncate

class PlayerCollider(
    width: Int,
    height: Int
) {
    val hurt = Hurt(width, height)
    val attacking = Attacking(width, height)

    class Hurt(width: Int, height: Int) {
        val normal = BoxCollider(truncate(width / 4f), truncate(height / 4f), (width / 2.5).toInt(), (height / 1.33).toInt())
        val jump = BoxCollider(truncate(width / 4f), truncate(height / 6.66f), (width / 2.5).toInt(), (height / 1.33).toInt())
        val sliding = BoxCollider(0f, truncate(height / 1.66f), (width / 1.42).toInt(), (height / 2.5).toInt())
    }

    class Attacking(width: Int, height: Int) {
        val normal = BoxCollider(truncate(width / 2f), truncate(height / 6.66f), (width / 2.5).toInt(), (height / 1.17f).toInt())
        val run = BoxCollider(truncate(width / 2f), truncate(height / 6.66f), (width / 3.33).toInt(), height / 2)
        val jump = BoxCollider(truncate(width / 2f), truncate(height / 10f), (width / 3.33).toInt(), (height / 1.66).toInt())
    }
}
