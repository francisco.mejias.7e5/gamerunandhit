package com.example.gamerunandhit.view

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.TypedArray
import android.graphics.*
import android.os.Build
import android.util.Size
import android.view.SurfaceView
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.core.graphics.blue
import androidx.core.graphics.green
import androidx.core.graphics.red
import androidx.navigation.findNavController
import com.example.gamerunandhit.R
import com.example.gamerunandhit.animation.Animation
import com.example.gamerunandhit.map.MinhScene
import com.example.gamerunandhit.model.Player
import com.example.gamerunandhit.model.Player.Action.*
import kotlinx.coroutines.*
import java.lang.Math.abs


@SuppressLint("ViewConstructor")
class GameView(context: Context, private val size: Size, returnHome: () -> Unit) : SurfaceView(context) {
    private var canvas: Canvas = Canvas()
    private val paint: Paint = Paint()
    private val scene = MinhScene(context, size.height, size.width)
    private val lifeWidth = (size.width / 2) / 11f

    private val player = Player(context, scene, returnHome, 50f)
    private val enemy = Player(context, scene, returnHome, size.width - 50f, sizeMultiplier = 3.0, countFromRight = true, sOrientation = LEFT)

    private val lotusAnimation = Animation(context,
        context.resources.obtainTypedArray(R.array.lotus_petals).toList().map {
            Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.resources, it), size.width, size.height, false)
        }
    )

    private fun TypedArray.toList(): List<Int> {
        val mutableList = mutableListOf<Int>()
        repeat(this.length()) {
            mutableList += this.getResourceId(it, 0)
        }
        return mutableList.toList()
    }

    private val lotus get() = lotusAnimation.get()

    init {
        startGame()
    }

    private fun startGame(){
        CoroutineScope(Dispatchers.Main).launch{
            while(this.isActive){
                draw()
                update()
                delay(10)
            }
        }
    }

    private fun draw(){
        if (holder.surface.isValid) {
            canvas = holder.lockCanvas()
            scene.drawMap(canvas, paint)

            canvas.drawLife(player.life, false)
            canvas.drawLife(enemy.life, true)

            canvas.drawBitmap(player.bitmap, player.positionX, player.positionY, paint)

            canvas.drawBitmap(enemy.bitmap, enemy.positionX, enemy.positionY, paint)

            canvas.drawBitmap(lotus, 0f, 0f, paint)

            holder.unlockCanvasAndPost(canvas)
        }
    }

    private fun Canvas.drawLife(life: Int, reversed: Boolean) {
        var color = ContextCompat.getColor(context, R.color.red_palette)
        val paint = paint
        paint.color = color

        repeat(life / 10) {
            repeat(10) {
                if (reversed) {
                    this.drawRect(size.width - (lifeWidth * (it + 1)), 0f, size.width - (lifeWidth * it), 50f, paint)
                } else {
                    this.drawRect(lifeWidth * it, 0f, lifeWidth * (it + 1), 50f, paint)
                }
            }
            color = Color.rgb(color.red - 25, 0, 0)
            paint.color = color
        }

        repeat(life % 10) {
            if (reversed) {
                this.drawRect(size.width - (lifeWidth * (it + 1)), 0f, size.width - (lifeWidth * it), 50f, paint)
            } else {
                this.drawRect(lifeWidth * it, 0f, lifeWidth * (it + 1), 50f, paint)
            }
        }
    }

    private fun update(){
        player.updatePlayer()
        enemy.enemyData = player.data
        enemy.updatePlayer()
        enemy.updateBot()
        player.enemyData = enemy.data
    }

    fun start(action: Player.Action) {
        player.start(action)
    }

    fun jump() {
        player.jump()
    }

    fun stop(action: Player.Action) {
        player.stop(action)
    }

    fun stopJump() {
        player.stop(JUMP)
        player.start(FALLING)
    }

    fun attack() {
        player.attack()
    }
}
