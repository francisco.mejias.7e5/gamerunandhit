package com.example.gamerunandhit.engine

import com.example.gamerunandhit.model.Player
import kotlin.math.log
import kotlin.math.min
import kotlin.math.round

class PhysicsEngine(
    private val ms: Int,
    private val fc: Int,
    private val iS: Float,
    private val artificial: Int,
    val gravity: Int,
    val jumpHeight: Float
) {
    internal var speedX = 0f
    internal var speedY = -50f

    private var jumpLooper = 0
    private var jumpMaxLooper = (jumpHeight / speedY).toInt() * -1

    internal fun calculateSpeed(lateral: Player.Action?, vertical: Player.Action?) {
        calculateX(lateral)
        calculateY(vertical)
    }

    private fun calculateX(lateral: Player.Action?) {
        var a = log((ms - speedX)/ms.toDouble(), 10.0) *-1 + iS + artificial
        if (lateral == Player.Action.LEFT) {
            a = log((ms - speedX * -1)/ms.toDouble(), 10.0) *-1 + iS + artificial
        }
        val r = (speedX/100)*fc/100*8
        var acr = a*fc/100
        if (lateral == null) {
            acr = 0.0
        }
        var vt = (round(acr*(ms - speedX)/10 * 1000.0) / 1000.0) - r
        if (lateral == Player.Action.LEFT) {
            vt = (round(acr*(-ms - speedX)/10 * 1000.0) / 1000.0) - r
        }
        speedX += vt.toFloat()
    }

    private fun calculateY(vertical: Player.Action?) {
        if (vertical == Player.Action.JUMP) {
            speedY = speedY*(jumpMaxLooper-jumpLooper)/jumpMaxLooper
            jumpLooper = min(jumpLooper + 1, jumpMaxLooper)
        } else {
            jumpLooper = 0
            speedY = -50f
        }
    }
}