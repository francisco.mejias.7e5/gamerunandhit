package com.example.gamerunandhit.model

import com.example.gamerunandhit.collider.Collider

class EnemyData(
    val colliderHurt: Collider?,
    val positionX: Float,
    val positionY: Float,
    val width: Int,
    val height: Int,
    val orientation: Player.Action?,
    val attacked: () -> Unit
)
