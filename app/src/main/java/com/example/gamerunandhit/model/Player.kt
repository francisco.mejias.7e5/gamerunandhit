package com.example.gamerunandhit.model

import com.example.gamerunandhit.collider.BoxCollider
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Point
import com.example.gamerunandhit.animation.Animation
import com.example.gamerunandhit.animation.PlayerAnimator
import com.example.gamerunandhit.collider.Collider
import com.example.gamerunandhit.collider.PlayerCollider
import com.example.gamerunandhit.engine.PhysicsEngine
import com.example.gamerunandhit.map.Scene
import com.example.gamerunandhit.model.Player.Action.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class Player(
    context: Context,
    private val scene: Scene,
    private val returnHome: () -> Unit,
    startingXPos: Float,
    sizeMultiplier: Double = 1.0,
    lifeMultiplier: Double = sizeMultiplier,
    countFromRight: Boolean = false,
    sOrientation: Action = RIGHT
) {
    lateinit var enemyData: EnemyData

    val width = (260 * sizeMultiplier).toInt()
    val height = (195 * sizeMultiplier).toInt()
    var life = (10 * lifeMultiplier).toInt()
    private val animator = PlayerAnimator(context, width, height)
    private var currentAnimation: Animation = animator.idle
        set(value) {
            currentAnimation.reset()
            value.orientation = currentAnimation.orientation
            field = value
        }
    val bitmap: Bitmap get() = currentAnimation.get()

    var positionX: Float = startingXPos + if (countFromRight) - width else 0
    var positionY: Float = scene.height - height.toFloat()
    var orientation = sOrientation
        set(value) {
            currentAnimation.orientation = value
            field = value
        }

    private val buffer = mutableSetOf<Action>()

    private val collider = PlayerCollider(width, height)
    var colliderBuffer: Pair<Collider?, Collider?> = null to null

    private var ms = 50
    private var fc = 130
    private var iS = 0.15f
    private var artificial = 0
    private var jumpHeight = scene.height - height * 2.2f
    private var gravity = 10
    private val physics = PhysicsEngine(
        ms,
        fc,
        iS,
        artificial,
        gravity,
        jumpHeight
    )

    val data get() = EnemyData(colliderBuffer.first, positionX, positionY, width, height, orientation) {
        attacked()
    }

    init {
        currentAnimation.orientation = orientation
        colliderBuffer = colliderBuffer.copy(first = collider.hurt.normal)
    }

    fun updatePlayer() {
        updatePosition()
        updateBitmap()
    }

    fun updateBot() {
        if (enemyData.positionX < this.positionX + width / 2) {
            start(LEFT)
            stop(RIGHT)
        }
        if (enemyData.positionX > this.positionX + width / 2) {
            start(RIGHT)
            stop(LEFT)
        }
        canIHitEnemy(collider.attacking.normal) {
            attack()
            stop(RIGHT)
            stop(LEFT)
        }
    }

    private fun canIHitEnemy(attackType: BoxCollider, attack: () -> Unit) {
        enemyData.colliderHurt?.let {
            if (attackType.onRelativeCollision(
                    it,
                    positionX,
                    positionY,
                    width,
                    enemyData.positionX,
                    enemyData.positionY,
                    enemyData.width,
                    orientation == LEFT,
                    enemyData.orientation == LEFT)
            ) {
                attack.invoke()
            }
        }
    }

    private fun updatePosition() {
        physics.calculateSpeed(buffer.find { it == LEFT || it == RIGHT }, buffer.find { it == JUMP })
        if (scene.isInSceneBounds(Point((positionX + physics.speedX).toInt(), positionY.toInt()), width, height)) {
            positionX += physics.speedX
        }

        if (scene.isInSceneBounds(Point(positionX.toInt(), (positionY.toInt() + (physics.speedY + physics.gravity)).toInt()), width, height)) {
            if (buffer has JUMP) {
                positionY += (physics.speedY - physics.gravity)

                if (positionY <= physics.jumpHeight) {
                    stop(JUMP)
                    start(FALLING)
                }
            } else if (buffer has FALLING) {
                if (!scene.isInSceneBounds(Point(positionX.toInt(), (positionY.toInt() + + physics.gravity)), width, height)) {
                    stop(FALLING)
                    positionY = (scene.height - height).toFloat()
                } else {
                    positionY += physics.gravity
                }
            }
        } else {
            stop(FALLING)
            positionY = (scene.height - height).toFloat()
        }
    }

    fun start(action: Action) {
        if (buffer hasnt DEAD) {
            buffer += action
        }
    }

    fun stop(action: Action) {
        buffer.remove(action)
    }

    private fun stopAll() {
        buffer.clear()
    }

    private fun updateBitmap() {
        when(buffer) {
            emptySet<Action>() -> {
                setCurrentAnimation(animator.idle)
            }
            setOf(DEAD) -> {
                setCurrentAnimation(animator.death)
                currentAnimation.onFinish {
                    currentAnimation.stop()
                    CoroutineScope(Dispatchers.Main).launch {
                        delay(100)
                        println("hey")
                        returnHome.invoke()
                    }
                }
            }
            setOf(JUMP) -> {
                setCurrentAnimation(animator.jumping)
            }
            setOf(ATTACK) -> {
                setCurrentAnimation(animator.slashing) {
                    launchAttack()
                }
            }
            setOf(ATTACK, JUMP) -> {
                setCurrentAnimation(animator.slashingInTheAir) {
                    launchAttack()
                }
            }
            setOf(ATTACK, FALLING) -> {
                setCurrentAnimation(animator.slashingInTheAir) {
                    launchAttack()
                }
            }
            setOf(LEFT) -> {
                setCurrentAnimation(animator.running)
                orientation = LEFT
            }
            setOf(LEFT, ATTACK) -> {
                setCurrentAnimation(animator.runSlashing) {
                    launchAttack()
                }
                orientation = LEFT
            }
            setOf(LEFT, JUMP) -> {
                setCurrentAnimation(animator.jumping)
                orientation = LEFT
            }
            setOf(LEFT, ATTACK, JUMP) -> {
                setCurrentAnimation(animator.slashingInTheAir) {
                    launchAttack()
                }
                orientation = LEFT
            }
            setOf(LEFT, ATTACK, FALLING) -> {
                setCurrentAnimation(animator.slashingInTheAir) {
                    launchAttack()
                }
                orientation = LEFT
            }
            setOf(RIGHT) -> {
                setCurrentAnimation(animator.running)
                orientation = RIGHT
            }
            setOf(RIGHT, ATTACK) -> {
                setCurrentAnimation(animator.runSlashing) {
                    launchAttack()
                }
                orientation = RIGHT
            }
            setOf(RIGHT, JUMP) -> {
                setCurrentAnimation(animator.jumping)
                orientation = RIGHT
            }
            setOf(RIGHT, ATTACK, JUMP) -> {
                setCurrentAnimation(animator.slashingInTheAir) {
                    launchAttack()
                }
                orientation = RIGHT
            }
            setOf(RIGHT, ATTACK, FALLING) -> {
                setCurrentAnimation(animator.slashingInTheAir) {
                    launchAttack()
                }
                orientation = RIGHT
            }
            else -> {}
        }
    }

    private fun setCurrentAnimation(animation: Animation, function: () -> Unit = {}) {
        if (currentAnimation.hashCode() != animation.hashCode()) {
            currentAnimation = animation
            function.invoke()
        }
    }

    private fun launchAttack() {
        currentAnimation.onFinish {
            stop(ATTACK)
            currentAnimation = animator.idle
            canIHitEnemy(collider.attacking.normal) {
                enemyData.attacked.invoke()
            }
        }
    }

    fun jump() {
        if (buffer hasnt FALLING) {
            start(JUMP)
        }
    }

    fun attack() {
        if (buffer hasnt ATTACK) {
            start(ATTACK)
        }
    }

    private fun attacked() {
        if (buffer hasnt DEAD) {
            life--
            if (life <= 0) {
                stopAll()
                start(DEAD)
            }
        }
    }

    enum class Action {
        LEFT,
        RIGHT,
        JUMP,
        FALLING,
        ATTACK,
        DEAD
    }

    private infix fun MutableSet<Action>.has(action: Action): Boolean =
        this.any { it == action }

    private infix fun MutableSet<Action>.has(setAction: Set<Action>): Boolean =
        this.any { setAction.contains(it) }

    private infix fun MutableSet<Action>.hasnt(action: Action): Boolean =
        !(this has action)

    private infix fun MutableSet<Action>.hasnt(setAction: Set<Action>): Boolean =
        !(this has setAction)
}