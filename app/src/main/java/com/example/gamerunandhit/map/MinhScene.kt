package com.example.gamerunandhit.map

import android.content.Context
import android.graphics.*
import androidx.core.graphics.toRectF
import com.example.gamerunandhit.R

class MinhScene(override val context: Context, override val height: Int, override val width: Int): Scene() {
    override val background: Bitmap = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.resources, R.drawable.background2), width, height,false)

    override fun drawMap(canvas: Canvas, paint: Paint) {
        paint.color = Color.BLACK
        canvas.drawBitmap(background, 0f, 0f, paint)
    }
}