package com.example.gamerunandhit.map

import android.content.Context
import android.graphics.*

sealed class Scene {
    abstract val context: Context
    abstract val height: Int
    abstract val width: Int
    private val bounds by lazy { Rect(0, 0, width, height) }
    abstract val background: Bitmap

    abstract fun drawMap(canvas: Canvas, paint: Paint)
    fun isInSceneBounds(point: Point, width: Int, height: Int): Boolean {
        return point.x in (bounds.left .. bounds.right - width) &&
                point.y in (bounds.top .. bounds.bottom - height)
    }
}