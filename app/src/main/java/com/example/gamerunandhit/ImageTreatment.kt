package com.example.gamerunandhit

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Matrix
import android.graphics.drawable.BitmapDrawable
import android.util.DisplayMetrics

object ImageTreatment {
    fun Bitmap.flip(resources: Resources): Bitmap {
        val m = Matrix()
        m.preScale(-1f, 1f)
        val dst = Bitmap.createBitmap(this, 0, 0, this.width, this.height, m, false)
        dst.density = DisplayMetrics.DENSITY_DEFAULT
        return BitmapDrawable(resources, dst).bitmap
    }
}